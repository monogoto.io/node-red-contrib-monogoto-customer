## _Monogoto Customer_ module for node-red

Allows communicating with the Monogoto Customer API from node-red.

This assumes you have [Node-RED](https://nodered.org) already installed and working, if you need to install Node-RED see [here](https://nodered.org/docs/getting-started/installation)

**NOTE:** This requires [Node.js](https://nodejs.org) v8.11.1+ and [Node-RED](https://nodered.org/) v0.19+.


### Installation

Install via Node-RED Manage Palette

```
node-red-contrib-monogoto-customer
```

Install via npm

```shell
$ cd ~/.node-red
$ npm node-red-contrib-monogoto-customer
# Restart node-red
```

### Usage 
* Drag the `monogoto customer` node from the `Monogoto` category of the node pallet, into your flow.
* Double tap the node instance and configure your username & password via the server configuration config-node.
* Choose the api & operation you would like to execute & fill in required parameters for the chosen API operation.
* **Note** in case the API request ended with an error, it will be passed on to a Node-RED `catch` node.
    
       
![alt text](example-form.png)

### Documentation
##### You may find documentation for each operation in the API by viewing the node info section in Node-RED
![alt text](documentation.png)


### Publishing
Publishing this package requires logging in to npm from the cli, the common way of doing that is to use [access tokens](https://docs.npmjs.com/creating-and-viewing-access-tokens).
Once a token is created it needs to be set as the `NPM_TOKEN` environment variable which is then used in the `.npmrc` file.
